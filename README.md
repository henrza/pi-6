# PI-6 - Estação Meteorológica de Baixo Custo

## Resumo

Este projeto tem como objetivo a construção de uma estação meteorológica de baixo custo usando os seguintes componentes:

1. ESP32 DevKit 1;
2. Sensor MQ135 (Nível de gases tóxicos)
3. Sensor BMP180 (Pressão atmosférica)
4. Sensor DHT11 (Temperatura ambiente)

Além disso, os valores coletados pelos sensores foram armazenados na plataforma da Adafruit IO (via protocolo MQTT) e depois coletados via API do Python para realizar análises gráficas usando o Jupyter Notebook, junto com Pandas e Matplotlib.
