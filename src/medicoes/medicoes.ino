/***********************************************************************************************************************
* PROJETO INTEGRADOR UNIVESP 2024
*
* DRP14-PJI510-SALA-001GRUPO-005
*
* Sistema de coleta de dados e envio para nuvem.
*
***********************************************************************************************************************/
#include <Adafruit_BMP085.h>
#include <MQ135.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include "config.h"

#define LEDR 16  //Leitura de dados
#define LEDG 17   
#define LED_CON   2  //Conectado à Nuvem
#define pin_MQ135 34
#define DHTPIN 4
#define DHTTYPE DHT11   // DHT 11  

MQ135 mq135_sensor(pin_MQ135);
Adafruit_BMP085 bmp;
DHT dht(DHTPIN, DHTTYPE);

// set up the 'altitude' feed
AdafruitIO_Feed *altitude = io.feed("altitude");

// set up the 'pressao' feed
AdafruitIO_Feed *pressao = io.feed("pressao");

// set up the 'temperatura' feed
AdafruitIO_Feed *temperatura = io.feed("temperatura");

// set up the 'counter' feed
AdafruitIO_Feed *counter = io.feed("counter");

//float temperature = 21.0; // Assume current temperature. Recommended to measure with DHT22
//float humidity = 25.0; // Assume current humidity. Recommended to measure with DHT22
float temp = dht.readTemperature();
float umid = dht.readHumidity();
int count = 1;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(LEDR, OUTPUT);
  pinMode(LEDG, OUTPUT);
  pinMode(DHTPIN, INPUT);
  digitalWrite(LEDR, LOW);
  digitalWrite(LEDG, LOW);
  //Config LED_CON
  pinMode(LED_CON, OUTPUT);



  if (!bmp.begin()) {
    Serial.println("Could not find a valid BMP085 sensor, check wiring!");
    while (1) {}
  }
}

void loop() {
// put your main code here, to run repeatedly:
  temp = dht.readTemperature();
  umid = dht.readHumidity();
  digitalWrite(LEDG, HIGH);
  Serial.print("Temperature (sensor BMP180)= ");
  Serial.print(bmp.readTemperature());
  Serial.println(" *C");

  Serial.print("Umidade (sensor DHT11)= ");
  Serial.print(umid);
  Serial.println(" %");

  Serial.print("Temperatura (sensor DHT11)= ");
  Serial.print(dht.readTemperature());
  Serial.println("  C");
  
  
  Serial.print("Pressure = ");
  Serial.print(bmp.readPressure());
  Serial.println(" Pa");
  
  // Calculate altitude assuming 'standard' barometric
  // pressure of 1013.25 millibar = 101325 Pascal
  Serial.print("Altitude = ");
  Serial.print(bmp.readAltitude());
  Serial.println(" meters");

  Serial.print("Pressure at sealevel (calculated) = ");
  Serial.print(bmp.readSealevelPressure());
  Serial.println(" Pa");

  // you can get a more precise measurement of altitude
  // if you know the current sea level pressure which will
  // vary with weather and such. If it is 1015 millibars
  // that is equal to 101500 Pascals.
  Serial.print("Real altitude = ");
  Serial.print(bmp.readAltitude(101500));
  Serial.println(" meters");

  float rzero = mq135_sensor.getRZero();
  float correctedRZero = mq135_sensor.getCorrectedRZero(temp, umid);
  float resistance = mq135_sensor.getResistance();
  float ppm = mq135_sensor.getPPM();
  float correctedPPM = mq135_sensor.getCorrectedPPM(temp, umid);

  Serial.print("MQ135 RZero: ");
  Serial.print(rzero);
  Serial.print("\t Corrected RZero: ");
  Serial.print(correctedRZero);
  Serial.print("\t Resistance: ");
  Serial.print(resistance);
  Serial.print("\t PPM: ");
  Serial.print(ppm);
  Serial.print("\t Corrected PPM: ");
  Serial.print(correctedPPM);
  Serial.println("ppm");
  umid = 0;
  temp = 0;

  digitalWrite(LEDG, LOW);
  delay(100);
  
  digitalWrite(LEDR, HIGH);
  Serial.println();
  delay(5000);
  digitalWrite(LEDR, LOW);
  delay(100);

  /***********************************************************************************************************************
  *
  *  A partir deste ponto, inicia-se a conexão com a nuvem e o envio dos dados
  *
  ***********************************************************************************************************************/
  int ContadorDeEnvios = 0;
  Serial.print("Connecting to Adafruit IO");

  // connect to io.adafruit.com
  io.connect();

  // wait for a connection
  while(io.status() < AIO_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
    
  digitalWrite(LED_CON, HIGH);


  // we are connected
  Serial.println();
  Serial.println(io.statusText());

  // io.run(); is required for all sketches.
  // it should always be present at the top of your loop
  // function. it keeps the client connected to
  // io.adafruit.com, and processes any incoming data.
  io.run();
    // save count to the 'counter' feed on Adafruit IO
  Serial.print("Enviando -> ");
  Serial.println(bmp.readAltitude(101500));
  altitude->save(bmp.readAltitude(101500));

  Serial.print("Enviando -> ");
  Serial.println(bmp.readTemperature());
  temperatura->save(bmp.readTemperature());

  Serial.print("Enviando -> ");
  Serial.println(bmp.readPressure());
  pressao->save(bmp.readPressure());

  // save count to the 'counter' feed on Adafruit IO
  Serial.print("Enviando -> ");
  Serial.println(count);
  counter->save(count);
  // increment the count by 1
  count ++;

  // Adafruit IO is rate limited for publishing, so a delay is required in
  // between feed->save events. In this example, we will wait three seconds
  // (1000 milliseconds == 1 second) during each loop.  

  delay(500);
  io.wifi_disconnect();
  
  //Verificação se o sistema desconectou
  if(io.status() < AIO_CONNECTED) digitalWrite(LED_CON, LOW);
  delay(30000);

}